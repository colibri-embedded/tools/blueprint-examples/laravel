# GraphQL Use Case #1
Subfolder `/graphql-uc1` contains graphql backend with following features:
- no authentication 
- no authorization

# GraphQL Use Case #2
Subfolder `/graphql-uc2` contains graphql backend with following features:
- authentication 
- authorization by ownership only

# GraphQL Use Case #3
Subfolder `/graphql-uc3` contains graphql backend with following features:
- authentication
- authorization by ownership and user role

# GraphQL Use Case #4
Subfolder `/graphql-uc4` contains graphql backend with following features:
- authentication 
- authorization by ownership and user role
- user is owned by another resource which is the top/main owner